export const API_STATES={
    LOADING: {
        status: 'loading',
        isLoading: true,
    },
    SUCCESS: {
        status: 'success',
        isLoading: false,
        isSuccess: true,
    },
    FAILURE: {
        status: 'failure',
        isLoading: false,
        isFailure: true,
    },
}
export const API_ENDPOINTS={
    RUN_QUERY: './api/query'
}
export const DEFAULT_QUERY = 'Select * from Customer;'
export const DEFAULT_ORDER_QUERY = 'Select * from Order;'
export const DEFAULT_LIMIT = "100";