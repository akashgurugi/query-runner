import * as CONSTANTS from './index.constants';

export function runQuery(query, limit) {
    return fetch(CONSTANTS.API_ENDPOINTS.RUN_QUERY+ '?' +new URLSearchParams({
        query,limit
    })).then(response => response.json());
}