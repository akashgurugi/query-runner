import * as CONSTANTS from './index.constants';

export function transformResponse(response){
    if(!response.columns){
        return CONSTANTS.API_STATES.FAILURE;
    }
    const columns = response.columns;
    return {
        ...CONSTANTS.API_STATES.SUCCESS,
        ...response,
        data: response.data.map((columnData)=>columns.map(column=>columnData[column]))
    }
}

export function getDefaultState(){
    return [
        {
            key: +new Date,
        }
    ]
}