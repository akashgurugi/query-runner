This is a dummy application (POC) which takes inspiration from 
[sql online compiler](https://www.programiz.com/sql/online-compiler/)

## Live Link

[Click Here](https://query-runner.vercel.app/)

## Overview

This is a POC application which tries to mimick online query runner behaviour.
Currently there are no query engine which validates the queries.
Features Set currently supported.

* This can run multiple queries

* Button to switch between the supported queries

* You can add multiple tabs to have multiple queries run simultaneously (Curently you can add only **5 session** at max)



At this moment it only supports three queries

* **Customer query** : any query string that contains **customer** (case insensitive). it will return the static data from customer schema.
    The schema returns following keys with their respective value
    ```
    "id",
    "first_name",
    "last_name",
    "email",
    "gender",
    "ip_address"
    ```


* **Order query** : any query string that contains **Order** (case insensitive). it will return the static data from customer schema
    The schema returns following keys with their respective values.
    ```
    "order_id",
    "currency",
    "address",
    "email"
    ```

* **Fallback query** : any query that doesn't match the above given query will return a fallback response
    The schema returns following keys with their respective values.
    ```
    "ndc_code",
    "company_name",
    "description",
    "nhs_number"
    ```



## Framework/Library Used

### Next JS [https://nextjs.org/docs]
React Based SSR framework. Its easy to get started with this 
also it gives you freedom to create API without any extra effort

### Bootstrap [https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css]
Bootstrap Added for Basic styling since coding css would require to much of effort .

### React-virtualized [https://bvaughn.github.io/react-virtualized]
Added this to improve performance. See Optimization Section.


## Page Speed and Optimisation.

While dealing with huge chunks of data. It is a primary requirement to maintain the page interaction Speed
Since this app support tab layout and each tab can at max contains **1000** records. Having 5 tabs and 
each having 1000 records might cause performance issues. To tackle this there are several solutions

* Adding pagination Support : This means user has to click next button or scroll to the last record then some new records will be fetched.This requires additional efforts on server as well as on client side. So didn't go with this approach.

* **Adding Virtualization (Currently added)**: This project currently is using this technique to impprove page performance. i'm using a package called [react-virtualized](https://bvaughn.github.io/react-virtualized).
    what this package does is it only adds the required nodes in dom which are visible rest of them are not part of DOM until they are visible.
    
**Frame rate Performance debugging**: 

    Without the Virtualization in place single tab was working with *okayish* Frame rate

    But when multiple tabs are added in page and dom is filled with more than 5k nodes.

    It started to negatively impact the layout when user tries to switch from tab to tab.

    Sometime the system used to lag (before Virtualization) 2 or maybe 3 seconds before the rendering happens.

    After adding the Virtualization library the frame rate is most of the times above 55FPS even while switching since the engine doesn't have to paint 1000 table rows as once.


**Different Time measurement (Lighthouse)** : Here are fixes i did to improve time related unit
First Contentful Paint: 1.2s (Green)
Time to Interactive: 2.6s (Green)
Speed Index: 1.2s (Green)
Largest Contentful Paint: 1.2 s (Green)
```
    Initially i was loading Bootstrap via CDN that was blocking the thread so moved that as npm dependency to save some time.
    I can even remove that to further improve performance.
    Removed some unnecessary code to further fasten the build process
    Some unused javascript is being added by Next.js (framework used). That unfortunately we can't eliminate.
```

Two problem that are still there 
* Unused javascript: Added by Next.js
* Unused CSS : Added by Bootstrap. If css written from scratch this can be saved.

Note: 
    I only tested for performance. Since it serve no purpose to check for PWA , SEO etc.
    Results mentioned are for Mobile device only since Desktop test run on better network simulation



**Scroll Related Issues** : As already covered solved these issues with Virtualization

Tools used for debugging

For page loading speed :
```
Lighthouse 
Page insight: https://pagespeed.web.dev/
Google dev tools
```

For Page performance: This includes usage performance like scroll with 1000 rows of data
```
Google dev tools:
    Frame rendering status
    Paint flashing
    scrolling performance issues
```




## API Reference

#### Get all items

```http
  GET /api/query
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `query` | `string` | if query string includes **customer** or  **order** keyword then it returns either customer data  or order data (mock) else it will return random data |
| `limit` | `string` | No of records to be returned max can be returned **1000** |




## Features that can be added

* Keyword highlight: Most of the query runners do have keywords highlight feature where the keywords of query language are highlighted with specific color.It can also added 

* Adding Loader: Currently Only text are being shown to user when "query is being executted". We can imporve the perceive performance by adding a skeleton screen or loader.

* Auto complete : Certain keywords can be auto completed / suggested for improve UX

* Overall UI : Currently the UI looks simple and its very difficult to understand the data 

* Export functionality: We can add export to CSV functionality for user

* Sort features by cilcking on table header: we can add a functionaliy where user can click on the table heading like **id** and we will sort in ascending order / descending order the query.

etc.