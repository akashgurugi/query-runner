import { useEffect, useRef, useState } from 'react';
import * as CONSTANTS from '../../../lib/index.constants';
import styles from './Query.module.css';

export default function Query({
  onChange,
  query: initialQuery,
  limit,
  setLimit,
}) {
  const ref = useRef();
  const [query, setQuery] = useState(initialQuery);
  const [defaultQuery, setDefaultQuery] = useState(
    initialQuery || CONSTANTS.DEFAULT_QUERY
  );

  function changeDefaultQuery(newQuery) {
    if (newQuery !== defaultQuery) {
      setDefaultQuery(newQuery);
      setQuery(newQuery);
      ref.current.innerHTML = newQuery;
    }
  }

  useEffect(() => {
    onChange(ref.current.innerHTML);
  }, [query, onChange, limit]);

  useEffect(() => {
    const node = ref.current;
    function handleChange() {
      setQuery(ref.current.innerHTML);
    }
    node.addEventListener('input', handleChange, { passive: true });
    return () => {
      node.removeEventListener('input', handleChange);
    };
  }, []);

  return (
    <>
      <span className={styles.comment}>
        -- Use the editor below to run queries or click the button to use
        existing query
      </span>
      <div className={styles.queriesButton}>
        <button
          className="btn btm-sm btn-outline-dark ml-2"
          onClick={() => changeDefaultQuery(CONSTANTS.DEFAULT_QUERY)}>
          Customer Query
        </button>
        <button
          className="btn btm-sm btn-outline-dark"
          onClick={() => changeDefaultQuery(CONSTANTS.DEFAULT_ORDER_QUERY)}>
          Order Query
        </button>
      </div>
      <div
        spellCheck={false}
        className={styles.query}
        contentEditable
        ref={ref}
        suppressContentEditableWarning>
        {defaultQuery}
      </div>
      <div className={styles.limitContainer}>
        limit:{' '}
        <input
          onChange={(ev) => setLimit(ev.target.value)}
          type="number"
          value={limit}
        />
      </div>
    </>
  );
}
