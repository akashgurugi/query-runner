import styles from './Table.module.css';
import { List } from 'react-virtualized';

export default function Table({ headings = [], rows = [] }) {
  return (
    <div className={styles.overflow}>
      <table className="">
            <div className={styles.tr}>
              {headings.map((colHead) => {
                return (
                  <th scope="col" key={colHead}>
                    {colHead}
                  </th>
                );
              })}
            </div>
          <List
            className=''
            rowHeight={30}
            width={typeof window !== "undefined" && window.innerWidth}
            height={1000}
            rowRenderer={({ index, key, style }) => {
              const columnData = rows[index];
              return (
                  <tr key={key} style={style}  className={styles.tr}>
                    {columnData.map((data, index) => (
                      <td key={index}>{data}</td>
                    ))}
                  </tr>
              );
            }}
            rowCount={rows.length}
            />
      </table>
    </div>
  );
}
