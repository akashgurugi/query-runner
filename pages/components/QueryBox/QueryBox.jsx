import { useState } from 'react';
import styles from './QueryBox.module.css';
import Query from '../Query';
import * as service from '../../../lib/index.service';
import * as utilities from '../../../lib/index.utilities';
import * as CONSTANTS from '../../../lib/index.constants';
import Table from '../Table';

export default function ShowQueryBox({
  setQueryResponse,
  query: initialQuery = CONSTANTS.DEFAULT_QUERY,
  response: initialResponse,
  index,
  limit: initialLimit = CONSTANTS.DEFAULT_LIMIT,
}) {
  const [query, setQuery] = useState(initialQuery);
  const [response, setResponse] = useState(initialResponse);
  const [limit, setLimit] = useState(initialLimit);
  async function runQuery() {
    setResponse(CONSTANTS.API_STATES.LOADING);
    try {
      const queryResponse = await service.runQuery(query, limit);
      const transformedResponse = utilities.transformResponse(queryResponse);
      setResponse(transformedResponse);
      setQueryResponse({
        index,
        response: transformedResponse,
        query: query,
        limit: limit
      });
    } catch (e) {
      console.log(e);
      setResponse(utilities.transformResponse(CONSTANTS.API_STATES.FAILURE));
    }
  }

  return (
    <>
      <div className={styles.queryBox}>
        <Query onChange={setQuery} limit={limit} query={query} setLimit={setLimit} />
        <button
          className={`btn btn-outline-dark ${styles.queryBtn} `}
          onClick={runQuery}
          disabled={response?.isLoading}>
          Run Query
        </button>
      </div>
      <div>
        {response?.isLoading && <span className='text-info'>Executing your Query !!!!</span>}
        {response?.isFailure && <span className='text-danger'>Fail to run query !!!!</span>}
      </div>
      {response?.isSuccess && response?.data?.length && (
        <Table headings={response.columns} rows={response.data} />
      )}
    </>
  );
}
