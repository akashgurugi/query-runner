import React from 'react';
import QueryBox from '../QueryBox';
import { useState } from 'react';
import styles from './QueryTabs.module.css';
import * as utilities from '../../../lib/index.utilities';

export default function Tabs() {
  const [activeTab, setActiveTab] = useState(0);
  const [tabs, setTabs] = useState(utilities.getDefaultState);
  function handleAddTab() {
    setTabs((tabs) => [
      ...tabs,
      {
        key: +new Date(),
      },
    ]);
    setActiveTab((activeTab) => activeTab + 1);
  }
  function setQueryResponse({ index, ...response }) {
    setTabs((tabs) => {
      const newObj = {
        ...tabs[index],
        ...response,
      };
      return [...tabs.slice(0, index), newObj, ...tabs.slice(index + 1)];
    });
  }
  const activeTabData = tabs[activeTab];
  return (
    <>
      <ul className="nav nav-tabs">
        {tabs.length < 5 ? (
          <li className="nav-item">
            <button className="btn btn-small" onClick={handleAddTab}>
              +
            </button>
          </li>
        ) : null}
        {tabs.map(({ key, query }, index) => {
          return (
            <li
              key={key}
              className={`nav-item ${styles.navItem} `}
              onClick={() => setActiveTab(index)}>
              <a
                className={`nav-link ${index === activeTab ? 'active' : ''} ${
                  styles.tabsLink
                }`}
                aria-current="page"
                href="#">
                {query ? query : `tab ${index + 1}`}
              </a>
            </li>
          );
        })}
      </ul>
      <QueryBox
        setQueryResponse={setQueryResponse}
        {...activeTabData}
        index={activeTab}
      />
    </>
  );
}
