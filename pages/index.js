import Head from 'next/head'
import styles from '../styles/Home.module.css'
import QueryTabs from './components/QueryTabs';

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Query Runner</title>
        <meta name="description" content="Mock Query runner. Read readme.md for complete details" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <QueryTabs />
      </main>
    </div>
  )
}
