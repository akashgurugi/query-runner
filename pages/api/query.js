import { userDetails, orderDetails,fallbackData } from './fixtures.json';
export default function handler(req, res) {
  const query = req.query.query;
  const limit = req.query.limit || 100;
  if (query.toLowerCase().includes("customer")) {
    return res.status(200).json({
      columns: userDetails.columns,
      data: userDetails.data.slice(0, limit)
    });
  }
  if (query.toLowerCase().includes("order")) {
    return res.status(200).json({
      columns: orderDetails.columns,
      data: orderDetails.data.slice(0, limit)
    });
  }
  return res.status(200).json({
    columns: fallbackData.columns,
    data: fallbackData.data.slice(0, limit)
  })
}
